package org.example.elshadKarimov.stackAndQueueInterviewQuestions;

public class ThreeInOne {
//    private int numberOfStacks = 3;
//    private int stackCapacity;
//    private int[] values;
//    private int[] sizes;
//
//    public ThreeInOne(int stackCapacity) {
//        this.stackCapacity = stackCapacity;
//        values = new int[stackCapacity * numberOfStacks];
//        sizes = new int[numberOfStacks];
//    }
//
//    public boolean isFull(int stackNum) {
//        if (sizes[stackNum] == stackCapacity) return true;
//        return false;
//    }
//
//    public boolean isEmpty(int stackNum) {
//        if (sizes[stackNum] == 0) return true;
//        return false;
//    }
//
//    private int indexOfTop(int stackNum) {
//        int offset = stackNum * stackCapacity;
//        int size = sizes[stackNum];
//        return offset + size - 1;
//    }
//
//    public void push(int stackNum, int value) {
//        if (isFull(stackNum)) {
//            System.out.println("Stack is full");
//        } else {
//            sizes[stackNum]++;
//            values[indexOfTop(stackNum)] = value;
//        }
//    }
//
//    public int pop(int stackNum) {
//        if (isEmpty(stackNum)) {
//            System.out.println("The stack is empty");
//            return -1;
//        } else {
//            int topIndex = indexOfTop(stackNum);
//            int value = values[topIndex];
//            values[topIndex] = 0;
//            sizes[stackNum]--;
//            return value;
//        }
//    }
//
//    public int peek(int stackNum) {
//        if (isEmpty(stackNum)) {
//            System.out.println("The stack is empty");
//            return -1;
//        } else {
//            return values[indexOfTop(stackNum)];
//        }
//    }

    private int numberOfStacks = 3;
    private int stackCapacity;
    private int[] values;
    private int[] sizes;

    public ThreeInOne(int stackCapacity) {
        this.stackCapacity = stackCapacity;
        this.values = new int[numberOfStacks * stackCapacity];
        this.sizes = new int[stackCapacity];
    }

    public boolean isFull(int stackNumber) {
        if (sizes[stackNumber] == stackCapacity) return true;
        return false;
    }

    public boolean isEmpty(int stackNumber) {
        if (sizes[stackNumber] == 0) return true;
        return false;
    }

    private int indexOfTop(int stackNumber) {
        int offset = stackNumber * stackCapacity;
        int size = sizes[stackNumber];
        return offset + size - 1;
    }

    public void push(int stackNumber, int value) {
        if (isFull(stackNumber)) {
            System.out.println("Stack is full");
        } else {
            sizes[stackNumber]++;
            values[indexOfTop(stackNumber)] = value;
        }
    }

    public int pop(int stackNumber) {
        if (isEmpty(stackNumber)) {
            System.out.println("Stack is empty");
            return -1;
        } else {
            int indexOfTop = indexOfTop(stackNumber);
            int value = values[indexOfTop];
            values[indexOfTop] = 0;
            sizes[stackNumber]--;
            return value;
        }
    }

    public int peek(int stackNumber) {
        if (isEmpty(stackNumber)) {
            System.out.println("Stack is empty");
            return -1;
        } else {
            return values[indexOfTop(stackNumber)];
        }
    }
}
