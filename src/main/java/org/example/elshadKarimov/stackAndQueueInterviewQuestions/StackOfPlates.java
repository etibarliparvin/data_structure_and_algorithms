package org.example.elshadKarimov.stackAndQueueInterviewQuestions;

import java.util.EmptyStackException;

public class StackOfPlates {

    private int capacity;
    public Node top;
    public Node bottom;
    public int size = 0;

    public StackOfPlates(int capacity) {
        this.capacity = capacity;
    }

    public boolean isFull() {
        return capacity == size;
    }

    public void join(Node above, Node below) {
        if (below != null) below.above = above;
        if (above != null) above.below = below;
    }

    public boolean push(int value) {
        if (size >= capacity) return false;
        size++;
        Node newNode = new Node(value);
        if (size == 1) bottom = newNode;
        join(newNode, top);
        top = newNode;
        return true;
    }

    public int pop() {
        if (top == null) throw new EmptyStackException();
        int result = top.value;
        top = top.below;
        size--;
        return result;
    }

    public int removeBottom() {
        Node b = bottom;
        bottom = bottom.above;
        if(bottom != null) bottom.below = null;
        size--;
        return b.value;
    }

    public class Node {
        public Node above;
        public Node below;
        public int value;

        public Node(int value) {
            this.value = value;
        }
    }
}
