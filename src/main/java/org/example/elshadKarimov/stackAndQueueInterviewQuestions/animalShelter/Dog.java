package org.example.elshadKarimov.stackAndQueueInterviewQuestions.animalShelter;

public class Dog extends Animal {

    public Dog(String name) {
        super(name);
    }

    @Override
    public String name() {
        return "Dog: " + name;
    }

}
