package org.example.elshadKarimov.queue;

public class CircularQueue2<T> {

     T[] array = (T[]) new Object[10];
     int size = 0;
     int end = 0;
     int front = 0;

    public CircularQueue2() {
    }

    public CircularQueue2(int size) {
        array = (T[]) new Object[size];
    }

    public boolean isFull() {
        return size == array.length;
    }

    public boolean isEmpty() {
        return size == 0;
    }

    public boolean insert(T element) {
        if (isFull()) return false;
        array[end++] = element;
        end = end % array.length;
        size++;
        return true;
    }

    public T remove() {
        if (isEmpty()) throw new RuntimeException("Queue is empty");
        T data = array[front++];
        front = front % array.length;
        size--;
        return data;
    }

    public int size() {
        return size;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("[");
        int i = front;
        do {
            sb.append(array[i++] + ", ");
            i %= array.length;
        } while (i != end);
        return sb.append("]").toString();
    }
}
