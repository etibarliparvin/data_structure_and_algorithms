package org.example.elshadKarimov.queue;

public class QueueArray<T> {

    private T[] array = (T[]) new Object[10];
    private int size = 0;
    private int topOfQueue = 0;
    private int beginningOfQueue = 0;

    public QueueArray() {
    }

    public QueueArray(int size) {
        this.array = (T[]) new Object[size];
    }

    public void enQueue(T element) {
        resize();
        array[topOfQueue++] = element;
        size++;
    }

    public T deQueue() {
        if (size == 0) return null;
        T result = array[beginningOfQueue++];
        if (beginningOfQueue == topOfQueue) {
            beginningOfQueue = topOfQueue = 0;
        }
        return result;
    }

    public T peek() {
        if (size == 0) return null;
        return array[beginningOfQueue];
    }

    public int size() {
        return this.size;
    }

    public void delete() {
        array = (T[]) new Object[10];
        size = beginningOfQueue = topOfQueue = 0;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("[");
        for (int i = beginningOfQueue; i < size; i++) {
            if (i < size - 1)
                sb.append(array[i]).append(", ");
            else sb.append(array[i]);
        }
        return sb.append("]").toString();
    }

    private void resize() {
        if (size == array.length - 1) {
            T[] newArray = (T[]) new Object[size + size / 2];
            for (int i = 0; i < size; i++) {
                newArray[i] = array[i];
            }
            array = newArray;
        }
    }
}
