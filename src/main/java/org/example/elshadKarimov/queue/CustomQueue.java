package org.example.elshadKarimov.queue;

public class CustomQueue<T> {

    private T[] array = (T[]) new Object[10];
    private int size = 0;
    private int end = 0;

    public CustomQueue() {
    }

    public CustomQueue(int size) {
        array = (T[]) new Object[size];
    }

    public boolean isFull() {
        return size == array.length;
    }

    public boolean isEmpty() {
        return size == 0;
    }

    public boolean insert(T element) {
        if (isFull()) return false;
        array[size++] = element;
        return true;
    }

    public T remove() {
        if (isEmpty()) throw new RuntimeException("Queue is empty");
        T data = array[0];
        // shift elements to the left
        for (int i = 1; i < size; i++) {
            array[i - 1] = array[i];
        }
        size--;
        return data;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("[");
        for (int i = 0; i < size; i++) {
            if (i < size - 1)
                sb.append(array[i]).append(", ");
            else sb.append(array[i]);
        }
        return sb.append("]").toString();
    }
}
