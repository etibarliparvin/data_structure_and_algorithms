package org.example.elshadKarimov.queue;

public class CircularQueue<T> {

    private T[] array = (T[]) new Object[10];
    private int size = 0;
    private int topOfQueue = -1;
    private int beginningOfQueue = -1;

    public CircularQueue() {
    }

    public CircularQueue(int size) {
        this.array = (T[]) new Object[size];
    }

    public void enQueue(T element) {
        if (isFull()) {
            System.out.println("The Queue is full");
        } else if (isEmpty()) {
            beginningOfQueue++;
            array[++topOfQueue] = element;
        } else {
            if (topOfQueue + 1 == size) topOfQueue = 0;
            else array[++topOfQueue] = element;
        }
    }

    public T deQueue() {
        if (isEmpty()) {
            System.out.println("The Queue is empty");
            return null;
        } else {
            T result = array[beginningOfQueue];
            array[beginningOfQueue] = null;
            if (beginningOfQueue == topOfQueue)
                beginningOfQueue = topOfQueue = -1;
            else if (beginningOfQueue + 1 == size)
                beginningOfQueue = 0;
            else beginningOfQueue++;
            return result;
        }
    }

    public T peek() {
        if (isEmpty()) {
            System.out.println("The Queue is empty");
            return null;
        }
        return array[beginningOfQueue];
    }

    public boolean isEmpty() {
        return topOfQueue == -1;
    }

    public boolean isFull() {
        if (topOfQueue + 1 == beginningOfQueue) return true;
        else if (beginningOfQueue == 0 && topOfQueue + 1 == size) return true;
        return false;
    }
}
