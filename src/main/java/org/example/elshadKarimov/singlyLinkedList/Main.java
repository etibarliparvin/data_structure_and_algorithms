package org.example.elshadKarimov.singlyLinkedList;

import org.example.elshadKarimov.doublyLinkedList.DoublyLinkedList;

public class Main {
    public static void main(String[] args) {
        DoublyLinkedList<String> list = new DoublyLinkedList<>();
        list.add("one"); // 0
        list.add("two"); // 1
        list.add("three"); // 2
        list.add("four"); // 3
        System.out.println(list.size());
        System.out.println(list);
        System.out.println("---------------------------------------------");
        list.remove(2);
        System.out.println(list.size());
        System.out.println(list);
        System.out.println("---------------------------------------------");

    }
}
