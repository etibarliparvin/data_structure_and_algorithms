package org.example.elshadKarimov.singlyLinkedList;

public class SinglyLinkedList<T> {

    private Node<T> head;
    private Node<T> tail;
    private int size = 0;

    public boolean add(T element) {
        return addLast(element);
    }

    public boolean add(T element, int index)   {
        if (index == 0) return add(element);
        if (index == size) return addLast(element);
        Node<T> found = getNodeByIndex(index - 1);
        Node<T> newNode = new Node<>(element);
        newNode.next = found.next;
        found.next = newNode;
        size++;
        return true;
    }

    public boolean addFirst(T element) {
        Node<T> newNode = new Node<>(element);
        newNode.next = head;
        head = newNode;
        if (tail == null)
            tail = head;
        size++;
        return true;
    }

    public boolean addLast(T element) {
        if (tail == null) return addFirst(element);
        Node<T> newNode = new Node<>(element);
        tail.next = newNode;
        tail = newNode;
        size++;
        return true;
    }

    public boolean remove(int index) {
        if (index == 0) {
            head = head.next;
            size--;
            if(size == 0) {
                tail = null;
            }
            return true;
        }
        Node<T> found = getNodeByIndex(index - 1);
        if (index == size - 1) {
            tail = found;
            found.next = null;
            size--;
            return true;
        }
        found.next = found.next.next;
        size--;
        return true;
    }

    public int size() {
        return size;
    }

    public void deleteAllList() {
        head = null;
        tail = null;
        size = 0;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < size; i++) {
            sb.append(getNodeByIndex(i).value);
            sb.append(" -> ");
        }
        return sb.toString();
    }

    private Node<T> getNodeByIndex(int index) {
        checkIndex(index);
        Node<T> node = head;
        for (int i = 0; i < index; i++) {
            node = node.next;
        }
        return node;
    }

    private void checkIndex(int index) {
        if (index < 0 || index >= size)
            throw new RuntimeException("Index out of bound exception");
    }

    private class Node<T> {
        private T value;
        private Node<T> next;

        public Node(T value) {
            this.value = value;
        }

        public Node(T value, Node<T> next) {
            this.value = value;
            this.next = next;
        }
    }
}
