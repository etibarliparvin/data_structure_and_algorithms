package org.example.elshadKarimov.singlyLinkedList;

public class CircularSinglyLinkedList<T> {

    private Node<T> head;
    private Node<T> tail;
    private int size;

    public boolean add(T element) {
        return addLast(element);
    }

    public boolean add(T element, int index) {
        if (index == 0) return addFirst(element);
        checkIndex(index);
        Node<T> found = getNodeByIndex(index - 1);
        Node<T> newNode = new Node<>(element);
        newNode.next = found.next;
        found.next = newNode;
        size++;
        return true;
    }

    public boolean addFirst(T element) {
        Node<T> newNode = new Node<>(element);
        newNode.next = head;
        head = newNode;
        if (tail == null)
            tail = head;
        tail.next = head;
        size++;
        return true;
    }

    public boolean addLast(T element) {
        if (tail == null) return addFirst(element);
        Node<T> newNode = new Node<>(element);
        tail.next = newNode;
        tail = newNode;
        tail.next = head;
        size++;
        return true;
    }

    public boolean remove(int index) {
        checkIndex(index);
        if (index == 0) {
            if (size == 1) {
                head = null;
                tail = null;
            } else {
                head = head.next;
                tail.next = head;
            }
        } else if (index == size - 1) {
            Node<T> found = getNodeByIndex(index - 1);
            tail = found;
            tail.next = head;
        } else {
            Node<T> found = getNodeByIndex(index - 1);
            found.next = found.next.next;
        }
        size--;
        return true;
    }

    public int size() {
        return size;
    }

    public void deleteAllList() {
        head = null;
        tail = null;
        size = 0;
    }

    public Node<T> getHead() {
        return head;
    }

    public Node<T> getTail() {
        return tail;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        Node<T> node = head;
        for (int i = 0; i < size; i++) {
            sb.append(node.value).append(" -> ");
            node = node.next;
        }
        return sb.toString();
    }

    private Node<T> getNodeByIndex(int index) {
        checkIndex(index);
        Node<T> node = head;
        for (int i = 0; i < index; i++)
            node = node.next;
        return node;
    }

    private void checkIndex(int index) {
        if (index < 0 || index >= size)
            throw new RuntimeException("Index out of bound exception");
    }

    private class Node<T> {
        private T value;
        private Node<T> next;

        public Node(T value) {
            this.value = value;
        }

        public Node(T value, Node<T> next) {
            this.value = value;
            this.next = next;
        }

        @Override
        public String toString() {
            return "Node{" +
                    "value=" + value +
                    ", next value=" + next.value +
                    '}';
        }
    }
}
