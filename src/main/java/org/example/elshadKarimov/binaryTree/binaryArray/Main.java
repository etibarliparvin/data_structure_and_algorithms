package org.example.elshadKarimov.binaryTree.binaryArray;

public class Main {
    public static void main(String[] args) {
        BinaryTree tree = new BinaryTree(9);
        tree.insert("n1");
        tree.insert("n2");
        tree.insert("n3");
        tree.insert("n4");
        tree.insert("n5");
        tree.insert("n6");
        tree.insert("n7");
        tree.insert("n8");
        tree.insert("n9");
        tree.preOrder(1);
    }
}
