package org.example.elshadKarimov.binaryTree.binaryNode;

public class BinaryNode {

    public String value;
    public BinaryNode left;
    public BinaryNode right;
    public int height;

}
