package org.example.elshadKarimov.linkedListCodingExercises;

public class Main {
    public static void main(String[] args) {
        SinglyLinkedList<String> list = new SinglyLinkedList<>();
        list.push("one");
        list.push("two");
        list.push("three");
        list.push("four");
        list.push("five");
        System.out.println(list);
        System.out.println(list.set(4, "X"));
        System.out.println(list);
    }
}
