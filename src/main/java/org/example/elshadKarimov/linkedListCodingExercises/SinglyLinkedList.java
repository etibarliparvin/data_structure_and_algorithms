package org.example.elshadKarimov.linkedListCodingExercises;

public class SinglyLinkedList<T> {

    private Node<T> head;
    private Node<T> tail;
    private int size = 0;

    public boolean push(T element) {
        return addLast(element);
    }

    public Node<T> pop() {
        if (size == 0) return null;
        if (size == 1) {
            Node<T> found = head;
            head = null;
            tail = null;
            size--;
            return found;
        }
        Node<T> node = tail;
        Node<T> found = getNodeByIndex(size - 2);
        found.next = null;
        tail = found;
        size--;
        return node;
    }

    public boolean add(T element) {
        return addLast(element);
    }

    public boolean add(T element, int index) {
        if (checkIndex(index)) {
            if (index == 0) {
                return addFirst(element);
            }
            Node<T> newNode = new Node<>(element);
            Node<T> found = getNodeByIndex(index - 1);
            newNode.next = found.next;
            found.next = newNode;
        }
        return false;
    }

    public boolean addFirst(T element) {
        Node<T> newNode = new Node<>(element);
        newNode.next = head;
        head = newNode;
        if (tail == null)
            tail = head;
        size++;
        return true;
    }

    public boolean addLast(T element) {
        if (tail == null) return addFirst(element);
        Node<T> newNode = new Node<>(element);
        tail.next = newNode;
        tail = newNode;
        size++;
        return true;
    }

    public boolean set(int index, T element) {
        if (checkIndex(index)) {
            if (size == 0) return false;
            if (size == 1) {
                head.value = element;
                tail.value = element;
                return true;
            }
            getNodeByIndex(index).value = element;
            return true;
        }
        return false;
    }

    public boolean remove(int index) {
        if (checkIndex(index)) {
            if (size == 0) return false;
            if (size == 1) {
                head = null;
                tail = null;
                size--;
                return true;
            }
            if (index == 0) {
                head = head.next;
                size--;
                return true;
            }
            Node<T> found = getNodeByIndex(index - 1);
            found.next = found.next.next;
            size--;
            return true;
        }
        return false;
    }

    public Node<T> get(int index) {
        return getNodeByIndex(index);
    }

    public Node<T> getHead() {
        return head;
    }

    public Node<T> getTail() {
        return tail;
    }

    public int size() {
        return this.size;
    }

    public void rotate(int number) {
        for (int i = 0; i < number; i++) {
            Node<T> node = getNodeByIndex(0);
            addLast(node.value);
            remove(0);
        }
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("[");
        Node<T> node = head;
        while (node != null) {
            if (node != tail) {
                sb.append(node.value).append(", ");
            } else {
                sb.append(node.value);
            }
            node = node.next;
        }
        sb.append("]");
        return sb.toString();
    }

    private Node<T> getNodeByIndex(int index) {
        if (checkIndex(index)) {
            Node<T> found = head;
            for (int i = 0; i < index; i++)
                found = found.next;
            return found;
        }
        return null;
    }

    private boolean checkIndex(int index) {
        if (index < 0 || index >= size)
            return false;
        return true;
    }

    public class Node<T> {
        private T value;
        private Node<T> next;

        public Node() {
        }

        public Node(T value) {
            this.value = value;
        }

        public Node(T value, Node<T> next) {
            this.value = value;
            this.next = next;
        }

        public T getValue() {
            return value;
        }

        public Node<T> getNext() {
            return next;
        }
    }
}
