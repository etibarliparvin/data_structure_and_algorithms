package org.example.elshadKarimov.stack;

import java.lang.reflect.Array;

public class MyStack<T extends Comparable<T>> {

    private T[] array = (T[]) Array.newInstance(Comparable.class, 10);
    private int size = 0;
    private T min = null;

    public MyStack() {
    }

    public MyStack(int size) {
        this.array = (T[]) Array.newInstance(Comparable.class, size);
    }

    public T push(T element) {
        resize();
        array[size++] = element;
        if (size == 1) {
            min = array[0];
        } else {
            if (array[size - 1].compareTo(min) <     0)
                min = array[size - 1];
        }
        return element;
    }

    public T pop() {
        T t = array[size - 1];
        array[size--] = null;
        return t;
    }

    public T peek() {
        return array[size - 1];
    }

    public int size() {
        return size;
    }

    public T getMin() {
        return min;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("[");
        for (int i = 0; i < size; i++) {
            if (i < size - 1)
                sb.append(array[i]).append(", ");
            else sb.append(array[i]).append("]");
        }
        return sb.toString();
    }

    private void resize() {
        if (size == array.length - 1) {
            T[] newArray = (T[]) Array.newInstance(Comparable.class, array.length * 2);
            for (int i = 0; i < size; i++) {
                newArray[i] = array[i];
            }
            array = newArray;
        }
    }
}
