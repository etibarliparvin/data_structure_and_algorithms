package org.example.elshadKarimov.stack;

import java.util.Stack;

public class Main {
    public static void main(String[] args) {
        MyStack<Integer> stack = new MyStack<>();
        stack.push(1);
        stack.push(7);
        stack.push(3);
        stack.push(5);
        System.out.println(stack.size());
        System.out.println(stack.getMin());
    }
}
