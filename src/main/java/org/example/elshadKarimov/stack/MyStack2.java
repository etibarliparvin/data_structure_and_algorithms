package org.example.elshadKarimov.stack;

public class MyStack2<T> {

    private Node<T> head;
    private Node<T> tail;
    private int size = 0;

    public T push(T element) {
        Node<T> newNode = new Node<>(element);
        if (head == null) {
            head = newNode;
        }
        if (tail == null)
            tail = newNode;
        else {
            tail.next = newNode;
            tail = newNode;
        }
        size++;
        return element;
    }

    public T pop() {
        if (size == 0) throw new RuntimeException("Stack is empty");
        else if (size == 1) {
            T value = head.value;
            head = null;
            tail = null;
            size--;
            return value;
        }
        T value = tail.value;
        Node<T> found = findNodeByIndex(size - 2);
        found.next = null;
        tail = found;
        size--;
        return value;
    }

    public T peek() {
        if (size == 0) throw new RuntimeException("Stack is empty");
        return tail.value;
    }

    public int size() {
        return size;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("[");
        Node<T> node = head;
        for (int i = 0; i < size; i++) {
            if (i < size - 1)
                sb.append(node.value).append(", ");
            else
                sb.append(node.value);
            node = node.next;
        }
        return sb.append("]").toString();
    }

    private Node<T> findNodeByIndex(int index) {
        Node<T> found = head;
        for (int i = 0; i < index; i++)
            found = found.next;
        return found;
    }

    private class Node<T> {
        private T value;
        private Node<T> next;

        public Node() {
        }

        public Node(T value) {
            this.value = value;
        }
    }
}
