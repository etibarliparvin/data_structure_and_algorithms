package org.example.elshadKarimov.recursion.kunal;

public class Main {
    public static void main(String[] args) {
        System.out.println(fibo(6));
    }

    public static int search(int[] arr, int target, int s, int e) {
        if (s > e) return -1;
        int m = s + (e - s) / 2;
        if (arr[m] == target) return m;
        if (target < arr[m])
            return search(arr, target, s, m - 1);
        return search(arr, target, m + 1, e);
    }

    public static int fibo(int n) {
        if (n < 2) return n;
        int first = fibo(n - 1);
        int second = fibo(n - 2);
        int sum = first + second;
        return sum;
//        return fibo(n - 1) + fibo(n - 2);
    }

    // tail recursion
    public static void foo(int n) {
        if (n == 5) {
            System.out.println(n);
            return;
        }
        System.out.println(n);
        foo(n++);
    }
}
