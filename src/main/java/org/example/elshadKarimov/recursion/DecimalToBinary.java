package org.example.elshadKarimov.recursion;

public class DecimalToBinary {
    public static void main(String[] args) {
        System.out.println(decimalToBinary(13));
        System.out.println(decimalToBinary(10));
        System.out.println(decimalToBinary(5));
        System.out.println(decimalToBinary(19));
    }

    public static int decimalToBinary(int n) {
        if(n == 0) return 0;
        return (n % 2) + decimalToBinary(n / 2) * 10;
    }
}
