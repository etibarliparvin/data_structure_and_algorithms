package org.example.elshadKarimov.recursion;

public class Factorial {
    public static void main(String[] args) {
        System.out.println(factorial(-4));
    }

    public static long factorial(int n) {
        if(n < 0) return -1;
//        if (n == 0) return 1;
//        return n * factorial(n - 1);
        return n == 1 ? 1 : n * factorial(n - 1);
    }
}
