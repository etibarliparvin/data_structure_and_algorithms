package org.example.elshadKarimov.recursion;

public class IsPalindrome {
    public static void main(String[] args) {
//        String name = "abcdef";
//        System.out.println(name.substring(1, name.length() - 1));
        System.out.println(isPalindrome("abcdef"));
    }

    public static boolean isPalindrome(String s) {
        if (s.isEmpty()) return true;
        if (s.charAt(0) != s.charAt(s.length() - 1)) return false;
        return isPalindrome(s.substring(1, s.length() - 1));
    }

    public boolean isPalindrome2(String s) {
        if (s.length() == 0 || s.length() == 1) return true;
        if (s.charAt(0) == s.charAt(s.length() - 1))
            return isPalindrome(s.substring(1, s.length() - 1));
        return false;
    }
}
