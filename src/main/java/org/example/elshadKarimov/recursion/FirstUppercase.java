package org.example.elshadKarimov.recursion;

public class FirstUppercase {
    public static void main(String[] args) {
        System.out.println(first("abcef"));
    }

    public static char first(String s) {
        if (s.isEmpty()) return ' ';
        if (s.charAt(0) > 64 && s.charAt(0) < 91) return s.charAt(0);
        return first(s.substring(1));
    }

    public static char first2(String str) {
        if (str.isEmpty()) {
            return ' ';
        }
        if (Character.isUpperCase(str.charAt(0))) {
            return str.charAt(0);
        } else {
            return first(str.substring(1));
        }
    }
}
