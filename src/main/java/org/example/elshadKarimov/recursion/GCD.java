package org.example.elshadKarimov.recursion;

public class GCD {
    public static void main(String[] args) {
        System.out.println(gcd(48, 18));
        System.out.println(gcd(32, 8));
        System.out.println(gcd(16, 5));
        System.out.println(gcd(18, 48));
    }

    public static int gcd(int a, int b) {
        if (b == 0) return a;
        return gcd(b, a % b);
    }
}
