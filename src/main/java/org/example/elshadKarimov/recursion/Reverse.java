package org.example.elshadKarimov.recursion;

public class Reverse {
    public static void main(String[] args) {
        System.out.println("Parvin".substring(1));
        System.out.println(reverse("Parvin"));
    }

    public static String reverse(String s) {
        if(s.isEmpty()) return s;
        return reverse(s.substring(1)) + s.charAt(0);
    }
}
