package org.example.elshadKarimov.recursion;

public class RecursiveRange {
    public static void main(String[] args) {
        System.out.println(recursiveRange(6));
    }

    public static int recursiveRange(int n) {
        if(n == 0) return n;
        return n + recursiveRange(n - 1);
    }
}
