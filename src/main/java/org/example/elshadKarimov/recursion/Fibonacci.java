package org.example.elshadKarimov.recursion;

public class Fibonacci {
    public static void main(String[] args) {
        System.out.println(fibonacci(4));
    }

    public static long fibonacci(int n) {
//        if (n == 0) return 0;
//        if(n == 1) return n;
        if (n == 0 || n == 1) return n;
        return fibonacci(n - 1) + fibonacci(n - 2);
    }
}
