package org.example.elshadKarimov.recursion;

public class FindingMaxInArray {
    public static void main(String[] args) {
        int[] arr = {5, 13, 11, 9};
        System.out.println(findingMax(arr, 3));
    }

    public static int findingMax(int[] arr, int n) {
        if (n == 0)
            return arr[0];
        return Math.max(arr[n], findingMax(arr, n - 1));
    }
}
