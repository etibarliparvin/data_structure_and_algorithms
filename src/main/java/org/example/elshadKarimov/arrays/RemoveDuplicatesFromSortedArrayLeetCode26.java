package org.example.elshadKarimov.arrays;

public class RemoveDuplicatesFromSortedArrayLeetCode26 {
    public static void main(String[] args) {
        int[] nums = {0, 0, 1, 1, 1, 2, 2, 3, 3, 4};
        System.out.println(removeDuplicates2(nums));
    }

    public static int removeDuplicates(int[] nums) {
        int result = 0;
        int n = nums.length;
        for (int i = 0; i < n - 1; i++) {
            if (nums[i] != nums[i + 1]) result++;
        }
        return ++result;
    }

    public static int removeDuplicates2(int[] nums) {
        if (nums.length == 0) {
            return 0;
        }

        int i = 0;
        for (int j = 1; j < nums.length; j++) {
            if (nums[j] != nums[i]) {
                i++;
                nums[i] = nums[j];
            }
        }

        return i + 1;
    }
}
