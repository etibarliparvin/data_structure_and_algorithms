package org.example.elshadKarimov.arrays;

import java.util.Arrays;

public class BestScore {
    public static void main(String[] args) {
        int[] myArray = {84,85,86,87,85,90,85,83,23,45,84,1,2,0};
        System.out.println(Arrays.toString(findTopTwoScores(myArray)));
    }

    public static int[] findTopTwoScores(int[] array) {
        int[] result = new int[2];
        int first = array[0], second = first;
        for (int i = 1; i < array.length; i++) {
            if (array[i] > first) {
                second = first;
                first = array[i];
            } else if(array[i] > second) {
                second = array[i];
            }
        }
        result[0] = first;
        result[1] = second;
        return result;
    }

    public static int[] findTopTwoScores2(int[] array) {
        int firstHighest = Integer.MIN_VALUE;
        int secondHighest = Integer.MIN_VALUE;

        for (int score : array) {
            if (score > firstHighest) {
                secondHighest = firstHighest;
                firstHighest = score;
            } else if (score > secondHighest && score < firstHighest) {
                secondHighest = score;
            }
        }

        return new int[]{firstHighest, secondHighest};
    }
}
