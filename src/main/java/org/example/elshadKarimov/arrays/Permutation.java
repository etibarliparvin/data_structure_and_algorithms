package org.example.elshadKarimov.arrays;

public class Permutation {
    public static void main(String[] args) {
        int[] a1 = {1, 300, 3, 4, 5};
        int[] a2 = {5, 4, 3, 300, 1};
        System.out.println(permutation3(a1, a2));
    }

    public static boolean permutation(int[] array1, int[] array2) {
        int n = array1.length;
        int count = 0;
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (array1[i] == array2[j]) {
                    count++;
                    break;
                }
            }
        }
        return count == n;
    }

    public static boolean permutation2(int[] array1, int[] array2) {
        int m1 = 1, m2 = 1, sum1 = 0, sum2 = 0, n = array1.length;
        for (int i = 0; i < n; i++) {
            sum1 += array1[i];
            m1 *= array1[i];
            sum2 += array2[i];
            m2 *= array2[i];
        }
        return sum1 - sum2 == 0 && m1 == m2;
    }

    public static boolean permutation3(int[] arr1, int[] arr2) {
        int[] count = new int[128];
        for (int i = 0; i < arr1.length; i++) {
            count[arr1[i]]++;
            count[arr2[i]]--;
        }
        for (int j : count) {
            if (j != 0) {
                return false;
            }
        }
        return true;
    }
}
