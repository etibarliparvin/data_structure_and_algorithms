package org.example.elshadKarimov.arrays;

import java.util.Arrays;

public class DuplicateNumber {
    public static void main(String[] args) {
        int[] arr = {5, 2, 6, 8, 6, 7, 7, 5, 2, 8};
        System.out.println(Arrays.toString(removeDuplicates2(arr)));
    }

    public static int[] removeDuplicates(int[] arr) {
        int index = 0;
        int n = arr.length;
        int size = 0;
        boolean duplicate = false;
        for (int i = 0; i < n; i++) {
            duplicate = false;
            for (int j = i + 1; j < n; j++) {
                if (arr[i] == arr[j]) {
                    duplicate = true;
                    break;
                }
            }
            if (!duplicate) size++;
        }
        int[] result = new int[size];
        for (int i = 0; i < n; i++) {
            duplicate = false;
            for (int j = i + 1; j < n; j++) {
                if (arr[i] == arr[j]) {
                    duplicate = true;
                    break;
                }
            }
            if (!duplicate) result[index++] = arr[i];
        }
        return result;
    }

    public static int[] removeDuplicates2(int[] arr) {
        int index = 0;
        int size = 0;
        int n = arr.length;
        int max = arr[0];
        for (int i = 1; i < n; i++) {
            max = Math.max(max, arr[i]);
        }

        int[] result = new int[max + 1];
        for (int i = 0; i < n; i++) {
            result[arr[i]]++;
        }

        for (int i = 0; i < max + 1; i++) {
            if (result[i] > 0)
                size++;
        }

        int[] newArray = new int[size];
        for (int i = 0; i < max + 1; i++) {
            if (result[i] > 0) newArray[index++] = i;
        }
        return newArray;
    }
}
