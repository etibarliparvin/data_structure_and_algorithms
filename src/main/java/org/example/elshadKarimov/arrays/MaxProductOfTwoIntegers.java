package org.example.elshadKarimov.arrays;

public class MaxProductOfTwoIntegers {
    public static void main(String[] args) {
        int[] arr = {1, 5, 3, 2, 5};
        System.out.println(maxProduct(arr));
    }

    public static String maxProduct(int[] intArray) {
        int first = intArray[0];
        int second = first;

        for (int i : intArray) {
            if (i > first) {
                second = first;
                first = i;
            } else if (i > second && i < first) {
                second = i;
            }
        }

        StringBuilder sb = new StringBuilder("[");
        sb.append(first);
        sb.append(", ");
        sb.append(second);
        sb.append("]");
        return sb.toString();
    }
}
