package org.example.elshadKarimov.arrays;

import java.util.Arrays;

public class RotateMatrix {
    public static void main(String[] args) {
        int[][] arr = {{1, 2, 3}, {4, 5, 6}, {7, 8, 9}};
        rotateMatrix(arr);
    }

    public static void rotateMatrix(int[][] matrix) {
        int n = matrix.length;
        int[][] arr = new int[n][n];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                arr[i][j] = matrix[n - j - 1][i];
            }
        }
        System.out.println(Arrays.deepToString(arr));
    }
}
