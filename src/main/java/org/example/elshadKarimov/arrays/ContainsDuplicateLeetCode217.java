package org.example.elshadKarimov.arrays;

import java.util.*;

public class ContainsDuplicateLeetCode217 {
    public static void main(String[] args) {
        Set<Integer> set = new HashSet<>();
        System.out.println(set.add(1));
        System.out.println(set.add(2));
        System.out.println(set.add(3));
        System.out.println(set.add(4));
        System.out.println(set.add(1));
    }

    public static boolean isUnique(int[] intArray) {
        Arrays.sort(intArray);
        for (int i = 0; i < intArray.length - 1; i++) {
            if (intArray[i] == intArray[i + 1])
                return false;
        }
        return true;
    }

    public static boolean isUnique2(int[] intArray) {
        int n = intArray.length;
        for (int i = 0; i < n - 1; i++) {
            for (int j = i + 1; j < n; j++) {
                if (intArray[i] == intArray[j])
                    return false;
            }
        }
        return true;
    }

    public static boolean isUnique3(int[] intArray) {
        Map<Integer, Integer> map = new HashMap<>();
        for (int i : intArray) {
            if (map.containsKey(i)) return false;
            map.put(i, i);
        }
        return true;
    }

    public static boolean isUnique4(int[] intArray) {
        Set<Integer> set = new HashSet<>();
        for (int i : intArray) {
            if (!set.add(i)) return false;
        }
        return true;
    }
}
