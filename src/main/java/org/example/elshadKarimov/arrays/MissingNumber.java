package org.example.elshadKarimov.arrays;

public class MissingNumber {
    public static void main(String[] args) {
        int[] myArray = {1,2,3,4,6};
        System.out.println(findMissingNumberInArray(myArray));
    }

    static int findMissingNumberInArray(int[] arr) {
        int sum = 0;
        int n = arr.length;
        for(int i = 0; i < n; i++) {
            sum += arr[i];
        }
        n++;
        n = (n * (n + 1)) / 2;
        return n - sum;
    }
}
