package org.example.elshadKarimov.arrays;

public class BestTimeToBuyAndSellStockLeetCode121 {
    public static void main(String[] args) {
        int[] prices = {7, 1, 5, 3, 6, 4};
        System.out.println(maxProfit(prices));
    }

    public static int maxProfit(int[] prices) { // O(n^2)
        int result = 0;
        int n = prices.length;
        for (int i = 0; i < n - 1; i++) {
            for (int j = i + 1; j < n; j++) {
                int dif = prices[j] - prices[i];
                result = Math.max(result, dif);
            }
        }
        return result;
    }

    public static int maxProfit2(int[] prices) { // O(n)
        int purchase = Integer.MAX_VALUE;
        int profit = 0;
        for (int price : prices) {
            if (price < purchase) {
                purchase = price;
            } else if (price - purchase > profit) {
                profit = price - purchase;
            }
        }
        return profit;
    }
}
