package org.example.elshadKarimov.arrays;

import java.util.Arrays;

public class MiddleFunction {
    public static void main(String[] args) {
        int[] array = {1, 2, 3, 4};
        int[] aa = middle(array);
        System.out.println(Arrays.toString(aa));
    }

    public static int[] middle(int[] array) {
        int[] newArray = new int[array.length - 2];
        for (int i = 1, j = 0; i < array.length - 1; i++)
            newArray[j++] = array[i];
        return newArray;
    }
}
