package org.example.elshadKarimov.doublyLinkedList;

import java.util.HashSet;
import java.util.Set;

public class CircularDoublyLinkedList<T extends Integer> {

    private Node<T> head;
    private Node<T> tail;
    private int size = 0;

    public boolean add(T element) {
        return addLast(element);
    }

    public boolean add(T element, int index) {
        checkIndex(index);
        if (index == 0) return addFirst(element);
        Node<T> newNode = new Node<>(element);
        if (index == size - 1) {
            Node<T> found = tail.previous;
            found.next = newNode;
            newNode.previous = found;
            newNode.next = tail;
            tail.previous = newNode;
            size++;
            return true;
        }
        Node<T> found = getNodeByIndex(index - 1);
        found.next.previous = newNode;
        newNode.next = found.next;
        found.next = newNode;
        newNode.previous = found;
        size++;
        return true;
    }

    public boolean addFirst(T element) {
        Node<T> newNode = new Node<>(element);
        if (head == null) {
            head = new Node<>();
        }
        newNode.next = head;
        head.previous = newNode;
        head = newNode;
        if (tail == null) {
            tail = head;
        }
        tail.next = head;
        head.previous = tail;
        size++;
        return true;
    }

    public boolean addLast(T element) {
        if (tail == null) return addFirst(element);
        Node<T> newNode = new Node<>(element);
        tail.next = newNode;
        newNode.previous = tail;
        newNode.next = head;
        tail = newNode;
        head.previous = tail;
        size++;
        return true;
    }

    public boolean remove(int index) {
        checkIndex(index);
        if (index == 0) {
            if (size == 1) {
                head = null;
                tail = null;
                size--;
                return true;
            }
            head = head.next;
            head.previous = tail;
            tail.next = head;
            size--;
            return true;
        }
        if (index == size - 1) {
            tail = tail.previous;
            tail.next = head;
            head.previous = tail;
            size--;
            return true;
        }
        Node<T> found = getNodeByIndex(index - 1);
        found.next.previous = null;
        found.next.next.previous = found;
        found.next = found.next.next;
        size--;
        return true;
    }

    public int size() {
        return size;
    }

    public void deleteAllList() {
        head.next.previous = null;
        head.next = null;
        head.previous = null;
        tail.next = null;
        tail.previous.next = null;
        tail.previous = null;
        size = 0;
    }

    public void removeDuplicates() {
        Set<T> set = new HashSet<>();
        for (int i = 0; i < size; i++) {
            Node<T> node = head;
            if (!set.add(node.value)) {
                remove(i);
            }
        }
    }

    public Node<T> nthToLast(int n) {
        Node<T> node = tail;
        for (int i = 0; i < n - 1; i++)
            node = node.previous;
        return node;
    }

    public void partition(T n) {
        Node<T> node = head;
        for (int i = 0; i < size; i++) {
            if (node.value.compareTo(n) < 0) {
                remove(i);
                addFirst(node.value);
            }
            node = node.next;
        }
    }

    public CircularDoublyLinkedList<? extends Integer> sumLists(CircularDoublyLinkedList<T> list1,
                                                                CircularDoublyLinkedList<T> list2) {
        CircularDoublyLinkedList<Integer> result = new CircularDoublyLinkedList<>();
        int sum1 = 0, sum2 = 0;
        Node<T> tail1 = list1.tail;
        Node<T> tail2 = list2.tail;
        for (int i = 0; i < list1.size; i++) {
            sum1 = sum1 * 10 + (int) tail1.value;
            sum2 = sum2 * 10 + (int) tail2.value;
            tail1 = tail1.previous;
            tail2 = tail2.previous;
        }
        sum1 += sum2;
        while (sum1 != 0) {
            int r = sum1 % 10;
            result.add(r);
            sum1 /= 10;
        }
        return result;
    }

//    public CircularDoublyLinkedList<? extends Integer> sumLists2(CircularDoublyLinkedList<T> list1,
//                                                                 CircularDoublyLinkedList<T> list2) {
//        CircularDoublyLinkedList<Integer> result = new CircularDoublyLinkedList<>();
//        int carry = 0;
//        Node<T> n1 = list1.head;
//        Node<T> n2 = list2.head;
//        int i1 = list1.size;
//        int i2 = list2.size;
//        while (i1 <= list1.size || i2 <= list2.size) {
//            int sum = carry;
//            if (i1 != list1.size) {
//                sum += (int) n1.value;
//                n1 = n1.next;
//                i1++;
//            }
//            if (i2 != list2.size) {
//                sum += (int) n2.value;
//                n2 = n2.next;
//                i2++;
//            }
//            result.addFirst(sum % 10);
//            carry = sum / 10;
//        }
//        return result;
//    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        Node<T> node = head;
        for (int i = 0; i < size; i++) {
            sb.append(node.value).append(" -> ");
            node = node.next;
        }
        return sb.toString();
    }

    private Node<T> getNodeByIndex(int index) {
        checkIndex(index);
        Node<T> node = head;
        for (int i = 0; i < index; i++) {
            node = node.next;
        }
        return node;
    }

    private void checkIndex(int index) {
        if (index < 0 || index >= size)
            throw new RuntimeException("Index out of bound exception");
    }

    public Node<T> getHead() {
        return head;
    }

    public Node<T> getTail() {
        return tail;
    }

    public class Node<T> {
        private T value;
        private Node<T> next;
        private Node<T> previous;

        public Node() {
        }

        public Node(T value) {
            this.value = value;
        }

        public Node(T value, Node<T> next, Node<T> previous) {
            this.value = value;
            this.next = next;
            this.previous = previous;
        }

        public T getValue() {
            return value;
        }

        public Node<T> getNext() {
            return next;
        }

        public Node<T> getPrevious() {
            return previous;
        }

        @Override
        public String toString() {
            return "Node{" +
                    "value=" + value +
                    '}';
        }
    }
}
