package org.example.elshadKarimov.doublyLinkedList;

public class Main {
    public static void main(String[] args) {
        System.out.println("Start");
        DoublyLinkedList<String> list1 = new DoublyLinkedList<>();
        list1.add("one");
        list1.add("two");
        list1.add("three");
        list1.add("four");
        list1.add("five");
        list1.add("six");

        DoublyLinkedList<String> list2 = new DoublyLinkedList<>();
        list2.add("three");
        list2.add("four");
        list2.add("five");
        list2.add("six");

        list1.addSameNode(list1, list2, "seven");
        list1.addSameNode(list1, list2, "eight");
        list1.addSameNode(list1, list2, "nine");

        list1.findIntersection(list1, list2);
        System.out.println("Finish");
    }
}
