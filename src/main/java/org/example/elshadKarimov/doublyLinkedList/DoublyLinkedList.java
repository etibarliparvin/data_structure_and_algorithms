package org.example.elshadKarimov.doublyLinkedList;

public class DoublyLinkedList<T> {

    private Node<T> head;
    private Node<T> tail;
    private int size = 0;

    public boolean add(T element) {
        return addLast(element);
    }

    public boolean add(T element, int index) {
        checkIndex(index);
        if (index == 0) return addFirst(element);
        Node<T> newNode = new Node<>(element);
        if (index == size - 1) {
            Node<T> found = tail.previous;
            found.next = newNode;
            newNode.previous = found;
            newNode.next = tail;
            tail.previous = newNode;
            size++;
            return true;
        }
        Node<T> found = getNodeByIndex(index - 1);
        newNode.next = found.next;
        found.next.previous = newNode;
        found.next = newNode;
        newNode.previous = found;
        size++;
        return true;
    }

    public boolean addFirst(T element) {
        if (head == null) {
            head = new Node<>();
        }
        Node<T> newNode = new Node<>(element);
        newNode.next = head;
        head.previous = newNode;
        head = newNode;
        if (tail == null) {
            tail = newNode;
        }
        size++;
        return true;
    }

    public boolean addLast(T element) {
        if (tail == null) return addFirst(element);
        Node<T> newNode = new Node<>(element);
        tail.next = newNode;
        newNode.previous = tail;
        tail = newNode;
        size++;
        return true;
    }

    public boolean remove(int index) {
        checkIndex(index);
        if (index == 0) {
            if (size == 1) {
                head = null;
                tail = null;
                size--;
                return true;
            }
            head = head.next;
            head.previous = null;
            size--;
            return true;
        } else if (index == size - 1) {
            tail = tail.previous;
            tail.next = null;
            size--;
            return true;
        }
        Node<T> found = getNodeByIndex(index - 1);
        found.next = found.next.next;
        found.next.previous = found;
        size--;
        return true;
    }

    public int size() {
        return size;
    }

    public void deleteAllList() {
        head.next.previous = null;
        head = null;
        tail.previous.next = null;
        tail = null;
        size = 0;
    }

    public void findIntersection(DoublyLinkedList<T> list1, DoublyLinkedList<T> list2) {
        if (list1.head == null || list2.head == null) return;
        if (list1.tail != list2.tail) return;
        Node<T> shorter = new Node<>();
        Node<T> longer = new Node<>();
        if (list1.size > list2.size) {
            longer = list1.head;
            shorter = list2.head;
        } else {
            longer = list2.head;
            shorter = list1.head;
        }
        longer = getNodeByIndex(Math.abs(list1.size - list2.size));
        while (shorter != longer) {
            shorter = shorter.next;
            longer = longer.next;
        }
        while (longer != null) {
            System.out.println(longer.value);
            longer = longer.next;
        }
    }

    public void addSameNode(DoublyLinkedList<T> llA, DoublyLinkedList<T> llB, T element) {
        Node<T> node = new Node<>(element);
        llA.tail.next = node;
        llA.tail = node;
        llA.size++;
        llB.tail.next = node;
        llB.tail = node;
        llB.size++;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        Node<T> node = head;
        for (int i = 0; i < size; i++) {
            sb.append(node.value).append(" -> ");
            node = node.next;
        }
        return sb.toString();
    }

    private Node<T> getNodeByIndex(int index) {
        checkIndex(index);
        Node<T> node = head;
        for (int i = 0; i < index; i++) {
            node = node.next;
        }
        return node;
    }

//    private Node<T> getKthNode(Node<T> head, int k) {
//        Node<T> current = head;
//        if (k > 0 && current != null) {
//            current = current.next;
//            k--;
//        }
//        return current;
//    }

    private void checkIndex(int index) {
        if (index < 0 || index >= size)
            throw new RuntimeException("Index out of bound exception");
    }

    public Node<T> getHead() {
        return head;
    }

    public Node<T> getTail() {
        return tail;
    }

    public int getSize() {
        return size;
    }

    public class Node<T> {
        private T value;
        private Node<T> next;
        private Node<T> previous;

        public Node() {
        }

        public Node(T value) {
            this.value = value;
        }

        public Node(T value, Node<T> next, Node<T> previous) {
            this.value = value;
            this.next = next;
            this.previous = previous;
        }

        public T getValue() {
            return value;
        }

        public Node<T> getNext() {
            return next;
        }

        public Node<T> getPrevious() {
            return previous;
        }

        @Override
        public String toString() {
            return "Node{" +
                    "value=" + value +
                    '}';
        }
    }
}
