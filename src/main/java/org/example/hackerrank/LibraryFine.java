package org.example.hackerrank;

public class LibraryFine {
    public static void main(String[] args) {
        System.out.println(libraryFine(15, 7, 2014, 1, 7, 2015));
    }
    public static int libraryFine(int d1, int m1, int y1, int d2, int m2, int y2) {
        if (y1 > y2) return 10_000;
        else if (y1 == y2 && m1 > m2) return (m1 - m2) * 500;
        else if (y1 == y2 && m1 == m2 && d1 > d2) return (d1 - d2) * 15;
        else return 0;
    }
}
