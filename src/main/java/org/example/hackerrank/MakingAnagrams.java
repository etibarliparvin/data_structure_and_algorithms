package org.example.hackerrank;

import java.util.HashMap;
import java.util.Map;

public class MakingAnagrams {
    public static void main(String[] args) {
        System.out.println(makeAnagram("fcrxzwscanmligyxyvym", "jxwtrhvujlmrpdoqbisbwhmgpmeoke"));
//        System.out.println(makeAnagram("ab", "aaab"));
    }

    public static int makeAnagram(String a, String b) {
        Map<Character, Integer> first = new HashMap<>();
        Map<Character, Integer> second = new HashMap<>();
        int result = 0;
        for (int i = 0; i < a.length(); i++) {
            if (first.containsKey(a.charAt(i))) {
                first.put(a.charAt(i), first.get(a.charAt(i)) + 1);
            } else {
                first.put(a.charAt(i), 1);
            }
        }
        for(int i = 0; i < b.length(); i++) {
            if(first.containsKey(b.charAt(i))) {
                first.put(b.charAt(i), first.get(b.charAt(i)) - 1);
            } else if(second.containsKey(b.charAt(i))){
                second.put(b.charAt(i), second.get(b.charAt(i)) + 1);
            } else {
                second.put(b.charAt(i), 1);
            }
        }
        first.putAll(second);
        for(Integer i : first.values()) {
            result += Math.abs(i);
        }
        return result;
    }
}
