package org.example.hackerrank;

public class ViralAdvertising {
    public static void main(String[] args) {
        System.out.println(viralAdvertising(5));
        System.out.println(viralAdvertising2(5));
    }
    public static int viralAdvertising(int n) {
        int received = 2;
        int result = 2;
        for (int i = 1; i < n; i++) {
            int newReceiver = received * 3;
            received = newReceiver / 2;
            result += received;
        }
        return result;
    }

    public static int viralAdvertising2(int n) {
        int people = 5;
        int sum = 0;
        for(int i = 0; i < n; i++) {
            people /= 2;
            sum += people;
            people *= 3;
        }
        return sum;
    }
}
