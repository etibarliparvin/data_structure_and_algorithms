package org.example.hackerrank;

import java.util.ArrayList;
import java.util.List;

public class DiagonalDifference {
    public static void main(String[] args) {
        List<List<Integer>> list = new ArrayList<>();
        List<Integer> a = List.of(11, 2, 4);
        List<Integer> b = List.of(4, 5, 6);
        List<Integer> c = List.of(10, 8, -12);
        list.add(a);
        list.add(b);
        list.add(c);
        System.out.println(diagonalDifference(list));
    }
    public static int diagonalDifference(List<List<Integer>> arr) {
        int sum1 = 0;
        int sum2 = 0;
        for(int i = 0; i < arr.size(); i++) {
            sum1 += arr.get(arr.size() - 1 - i).get(i);
            sum2 += arr.get(i).get(i);
        }
        return Math.abs(sum1 - sum2);
    }
}
