package org.example.hackerrank;

import java.util.List;

public class PlusMinus {
    public static void plusMinus(List<Integer> arr) {
        // Write your code here
        int positive = 0;
        int zero = 0;
        int negative = 0;
        int n = arr.size();
        for (Integer i : arr) {
            if (i > 0)
                positive++;
            else if (i == 0)
                zero++;
            else
                negative++;
        }
        double p = (double) positive / n;
        double z = (double) zero / n;
        double ne = (double) negative / n;
        System.out.printf("%.6f\n", p);
        System.out.printf("%.6f\n", ne);
        System.out.printf("%.6f\n", z);
    }
}
