package org.example.hackerrank;

public class ElectronicShop {
    static int getMoneySpent(int[] keyboards, int[] drives, int b) {
        int result = -1;
        for (int i = 0; i < keyboards.length; i++) {
            for (int j = 0; j < drives.length; j++) {
                int sum = keyboards[i] + drives[j];
                if (sum <= b && sum > result) {
                    result = sum;
                }
            }
        }
        return result;
    }
}
