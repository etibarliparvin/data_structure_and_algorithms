package org.example.hackerrank;

import java.util.List;

public class SimpleArraySum {
    public static void main(String[] args) {

    }

    public static int simpleArraySum(List<Integer> ar) {
        int sum = 0;
        for (Integer i : ar) {
            sum += i;
        }
        return sum;
    }
}
