package org.example.hackerrank;

import java.util.List;

public class BetweenTwoSets {
    public static void main(String[] args) {
        System.out.println(getTotalX(List.of(2, 4), List.of(16, 32, 96)));
    }

    public static int getTotalX(List<Integer> a, List<Integer> b) {
        int count = 0;
        int begin = a.get(a.size() - 1);
        int end = b.get(0);
        for (int i = begin; i <= end; i += begin) {
            boolean arrayA = true;
            boolean arrayB = true;
            for (int j = 0; j < a.size() - 1; j++) {
                if (i % a.get(j) != 0) {
                    arrayA = false;
                    break;
                }
            }
            if (arrayA) {
                for (int k = 0; k < b.size(); k++) {
                    if (b.get(k) % i != 0) {
                        arrayB = false;
                        break;
                    }
                }
            }
            if (arrayA && arrayB) count++;
        }
        return count;
    }
}
