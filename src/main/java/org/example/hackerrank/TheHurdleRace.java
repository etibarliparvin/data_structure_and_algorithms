package org.example.hackerrank;

import java.util.List;

public class TheHurdleRace {
    public static int hurdleRace(int k, List<Integer> height) {
        int max = 0;
        for (Integer i : height) {
            if (i > max) max = i;
        }
        if(max > k) return max - k;
        return 0;
    }
}
