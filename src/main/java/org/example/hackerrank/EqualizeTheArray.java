package org.example.hackerrank;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class EqualizeTheArray {
    public static int equalizeArray(List<Integer> arr) {
        // Write your code here
        Map<Integer, Integer> map = new HashMap<>();
        int count = 0;
        for (int i = 0; i < arr.size(); i++) {
            if (map.containsKey(arr.get(i))) {
                map.put(arr.get(i), map.get(arr.get(i)) + 1);
            } else {
                map.put(arr.get(i), 1);
            }
        }
        Collection<Integer> values = map.values();
        for(Integer i : values) {
            if(i > count) count = i;
        }
        return arr.size() - count;
    }
}
