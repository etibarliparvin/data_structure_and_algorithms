package org.example.hackerrank;

import java.util.List;

public class MiniMaxSum {
    public static void miniMaxSum(List<Integer> arr) {
        // Write your code here
        long min = arr.get(0);
        long max = arr.get(0);
        long sum = arr.get(0);
        for (int i = 1; i < arr.size(); i++) {
            sum += arr.get(i);
            if (arr.get(i) < min) min = arr.get(i);
            if (arr.get(i) > max) max = arr.get(i);
        }
        System.out.printf("%s %s", sum - max, sum - min);
    }
}
