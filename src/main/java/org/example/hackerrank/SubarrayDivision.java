package org.example.hackerrank;

import java.util.List;

public class SubarrayDivision {
    public static void main(String[] args) {
        System.out.println(birthday(List.of(2, 5, 1, 3, 4, 4, 3, 5, 1, 1, 2, 1, 4, 1, 3, 3, 4, 2, 1), 18, 7));
    }

    public static int birthday(List<Integer> s, int d, int m) {
        int count = 0;
        int sum = 0;
        for (int i = 0; i < m; i++) {
            sum += s.get(i);
        }
        if (sum == d) count++;
        for (int i = m, j = 0; i < s.size(); j++, i++) {
            sum += s.get(i) - s.get(j);
            if (sum == d) count++;
        }
        return count;
    }
}
