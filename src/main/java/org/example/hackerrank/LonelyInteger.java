package org.example.hackerrank;

import java.util.List;

public class LonelyInteger {
    public static void main(String[] args) {
        System.out.println(lonelyinteger(List.of(4, 9, 95, 93, 57, 4, 57, 93, 9)));
    }

    public static int lonelyinteger(List<Integer> a) {
        int number = a.get(0);
        int[] numbers = new int[100];
        for (int i = 0; i < a.size(); i++) {
            numbers[a.get(i)]++;
        }
        for (int i = 0; i < 100; i++) {
            if (numbers[i] == 1) {
                number = i;
                break;
            }
        }
        return number;
    }
}
