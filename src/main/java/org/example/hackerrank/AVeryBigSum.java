package org.example.hackerrank;

import java.util.List;

public class AVeryBigSum {
    public static void main(String[] args) {

    }

    public static long aVeryBigSum(List<Long> ar) {
        long sum = 0;
        for (Long l : ar) {
            sum += l;
        }
        return sum;
    }
}
