package org.example.hackerrank;

import java.util.ArrayList;
import java.util.List;

public class BreakingTheRecords {
    public static List<Integer> breakingRecords(List<Integer> scores) {
        // Write your code here
        int countBest = 0;
        int countWorst = 0;
        int max = scores.get(0);
        int min = max;
        for (Integer i : scores) {
            if (i > max) {
                countBest++;
                max = i;
            }
            if (i < min) {
                countWorst++;
                min = i;
            }
        }
        List<Integer> list = new ArrayList<>();
        list.add(countBest);
        list.add(countWorst);
        return list;
    }
}
