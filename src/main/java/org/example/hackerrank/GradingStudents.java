package org.example.hackerrank;

import java.util.ArrayList;
import java.util.List;

public class GradingStudents {
    public static List<Integer> gradingStudents(List<Integer> grades) {
        List<Integer> result = new ArrayList<>();
        for (Integer i : grades) {
            if (i < 38) {
                result.add(i);
            } else if (i % 5 == 4) {
                result.add(i + 1);
            } else if (i % 5 == 3) {
                result.add(i + 2);
            } else {
                result.add(i);
            }
        }
        return result;
    }
}
