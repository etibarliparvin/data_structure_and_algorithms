package org.example.hackerrank;

public class BeautifulDaysAtTheMovies {
    public static void main(String[] args) {
        System.out.println(beautifulDays(20, 23, 6));
    }

    public static int beautifulDays(int i, int j, int k) {
        int count = 0;
        while (i <= j) {
            int result = 0;
            int n = i;
            while (n != 0) {
                int remainder = n % 10;
                result = result * 10 + remainder;
                n /= 10;
            }
            if (Math.abs(i - result) % k == 0) count++;
            i++;
        }
        return count;
    }
}
