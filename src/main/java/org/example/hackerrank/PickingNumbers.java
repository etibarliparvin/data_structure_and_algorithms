package org.example.hackerrank;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class PickingNumbers {

    public static void main(String[] args) {
        List<Integer> list = new ArrayList<>();
        list.add(4);
        list.add(6);
        list.add(5);
        list.add(3);
        list.add(3);
        list.add(1);
        System.out.println(pickingNumbers(list));
        System.out.println(pickingNumbers2(list));
        System.out.println(pickingNumbers3(list));
    }

    public static int pickingNumbers(List<Integer> a) {
        int ans = 0;
        Collections.sort(a);
        for (int i = 0; i < a.size(); i++) {
            int count = 1;
            for (int j = i + 1; j < a.size(); j++) {
                int diff = a.get(j) - a.get(i);
                if (diff <= 1) {
                    count++;
                }
            }
            ans = Math.max(ans, count);
        }
        return ans;
    }

    public static int pickingNumbers2(List<Integer> a) {
        Collections.sort(a);
        int val = a.get(0);
        int count = 1;
        int max = 1;
        for (int i = 1; i < a.size(); i++) {
            if (a.get(i) - val > 1) {
                val = a.get(i);
                count = 1;
            } else {
                count++;
                if (count > max)
                    max = count;
            }
        }
        return max;
    }

    public static int pickingNumbers3(List<Integer> a) {
        int max = 1;
        int[] freq = new int[101];
        for (Integer i : a) {
            freq[i]++;
        }
        for (int i = 0; i < 101; i++) {
            if (freq[i] == 0) continue;
            if (freq[i] + freq[i + 1] > max)
                max = freq[i] + freq[i + 1];
        }
        return max;
    }
}
