package org.example.hackerrank;

public class TraumAndBday {
    public static long taumBday(int b, int w, int bc, int wc, int z) {
        if (wc > bc + z) {
            return (long) w * (bc + z) + (long) b * bc;
        } else if (bc > wc + z) {
            return (long) b * (wc + z) + (long) w * wc;
        } else {
            return ((long) b * bc) + ((long) w * wc);
        }
    }
}
