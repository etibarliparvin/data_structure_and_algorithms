package org.example.hackerrank;

public class ChocolateFeast {
    public static void main(String[] args) {
        System.out.println(chocolateFeast(10, 2, 5));
        System.out.println(chocolateFeast(12, 4, 4));
        System.out.println(chocolateFeast(6, 2, 2));
    }
    public static int chocolateFeast(int n, int c, int m) {
        int count = 0;
        int wrapper = n / c;
        count += wrapper;
        while (wrapper >= m) {
            int remainder = wrapper % m;
            wrapper /= m;
            count+= wrapper;
            wrapper += remainder;
        }
        return count;
    }
}
