package org.example.hackerrank;

public class SaveThePrisoner {
    public static void main(String[] args) {
        System.out.println(saveThePrisoner(3, 394274638, 3));
    }
    public static int saveThePrisoner(int n, int m, int s) {
        int result = m % n + s - 1;
        if(result == 0) result = n;
        else if(result > n) result %= n;
        return result;
    }
}
