package org.example.hackerrank;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class BigSorting {
    public static void main(String[] args) {
        System.out.println(bigSorting(List.of("1", "200", "150", "3")));
    }
    public static List<String> bigSorting(List<String> unsorted) {
        List<BigInteger> list = new ArrayList<>();
        List<String> result = new ArrayList<>();
        for (String s : unsorted) {
            list.add(new BigInteger(s));
        }
        Collections.sort(list);
        for (BigInteger i : list) {
            result.add(String.valueOf(i));
        }
        return result;
    }
}
