package org.example.hackerrank;

public class HalloweenSale {
    public static void main(String[] args) {
        System.out.println(howManyGames(20, 3, 6, 100));
        System.out.println(howManyGames2(20, 3, 6, 100));
    }

    public static int howManyGames(int p, int d, int m, int s) {
        int countOfIteration = 0;
        int count = 0;
        while (s >= p) {
            countOfIteration++;
            count++;
            s -= p;
            p -= d;
            if (p < m) p = m;
        }
        return countOfIteration;
    }

    public static int howManyGames2(int p, int d, int m, int s) {
        int countOfIteration = 0;
        int count = 0;
        while (s >= p) {
            countOfIteration++;
            count++;
            s -= p;
            p -= d;
            if (p < m) {
                p = m;
                count += s / p;
                s %= p;
            }
        }
        return countOfIteration;
    }
}
