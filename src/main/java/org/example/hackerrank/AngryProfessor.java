package org.example.hackerrank;

import java.util.List;

public class AngryProfessor {
    public static void main(String[] args) {
        System.out.println(angryProfessor(3, List.of(-1, -3, 4, 2)));
        System.out.println(angryProfessor(2, List.of(0, -1, 2, 1)));
    }

    public static String angryProfessor(int k, List<Integer> a) {
        for (Integer i : a) {
            if (i < 1) k--;
        }
        return k < 1 ? "NO" : "YES";
    }
}
