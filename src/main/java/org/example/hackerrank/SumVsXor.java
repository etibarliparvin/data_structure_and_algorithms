package org.example.hackerrank;

public class SumVsXor {
    public static void main(String[] args) {
        System.out.println(sumXor(100));
    }

    public static long sumXor(long n) {
        int count = 0;
        String number = Long.toBinaryString(n);
        for (int i = 0; i < number.length(); i++) {
            if (number.charAt(i) == '0') {
                count++;
            }
        }
        return (n != 0) ? (long) Math.pow(2, count) : 1;
    }
}
