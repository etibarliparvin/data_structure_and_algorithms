package org.example.hackerrank;

public class TimeConversion {
    public static void main(String[] args) {
        System.out.println(timeConversion("06:40:03AM"));
        System.out.println(timeConversion("12:45:54PM"));
        System.out.println(timeConversion("04:59:59AM"));
    }

    public static String timeConversion(String s) {
        int n = s.length();
        String sub = s.substring(n - 2);
        if (s.substring(0, 2).equals("12") && sub.equals("AM")) {
            return "00" + s.substring(2, n - 2);
        }
        if (s.substring(0, 2).equals("12") && sub.equals("PM")) {
            return s.substring(0, n - 2);
        }
        if (sub.equals("PM")) {
            int hours = Integer.parseInt(s.substring(0, 2)) + 12;
            return hours + "" + s.substring(2, n - 2);
        }
        return s.substring(0, n - 2);
    }
}
