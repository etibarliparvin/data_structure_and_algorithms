package org.example.hackerrank;

import java.util.Collections;
import java.util.List;

public class NewYearChaos {
    public static void main(String[] args) {

    }

    public static void minimumBribes(List<Integer> q) {
        int bribes = 0;
        for (int i = q.size(); i > 0; i--) {
            if (i == q.get(i - 1)) {
                continue;
            }
            if (i == q.get(i - 2)) {
                bribes++;
                Collections.swap(q, i - 1, i - 2);
                continue;
            }
            if (i == q.get(i - 3)) {
                bribes += 2;
                Collections.swap(q, i - 3, i - 2);
                Collections.swap(q, i - 2, i - 1);
                continue;
            }
            System.out.println("Too chaotic");
            return;
        }
        System.out.println(bribes);
    }
}
