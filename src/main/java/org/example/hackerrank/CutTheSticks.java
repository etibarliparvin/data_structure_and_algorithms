package org.example.hackerrank;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CutTheSticks {
    public static void main(String[] args) {
        List<Integer> list = new ArrayList<>();
        list.add(5);
        list.add(4);
        list.add(4);
        list.add(2);
        list.add(2);
        list.add(8);
        System.out.println(cutTheSticks(list));
    }

    public static List<Integer> cutTheSticks(List<Integer> arr) {
        Collections.sort(arr);
        List<Integer> result = new ArrayList<>();
        int temp = 0;
        for (int i = 0; i < arr.size(); i++) {
            if (temp != arr.get(i)) {
                result.add(arr.size() - i);
                temp = arr.get(i);
            }
        }
        return result;
//        Collections.sort(arr);
//        List<Integer> result = new ArrayList<>();
//        result.add(arr.size());
//        Map<Integer, Integer> map = new HashMap<>();
//        for (int i = 0; i < arr.size(); i++) {
//            if (map.containsKey(arr.get(i))) {
//                map.put(arr.get(i), map.get(arr.get(i)) + 1);
//            } else {
//                map.put(arr.get(i), 1);
//            }
//        }
//        int k = 0;
//        for (Integer i : map.values()) {
//            result.add(result.get(k++) - i);
//        }
//        result.remove(result.size() - 1);
//        return result;
    }
}
