package org.example.hackerrank;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class BeautifulTriplets {
    public static void main(String[] args) {
//        System.out.println(beautifulTriplets(3, List.of(1, 2, 4, 5, 7, 8, 10)));
//        System.out.println(beautifulTriplets(1, List.of(2, 2, 3, 4, 5)));
        System.out.println(beautifulTriplets(3, List.of(1, 6, 7, 7, 8, 10, 12, 13, 14, 19)));
    }

    public static int beautifulTriplets(int d, List<Integer> arr) {
        Map<Integer, Integer> map = new HashMap<>();
        int result = 0;
        for (int i = 0; i < arr.size(); i++) {
            int val = arr.get(i);
            if (map.containsKey(val - d) && map.containsKey(val - (d * 2))) {
                result += Math.max(map.get(val - d), map.get(val - (d * 2)));
            }
            map.put(val, map.getOrDefault(val, 0) + 1);
        }
        return result;
    }

    public static int beautifulTriplets2(int d, List<Integer> arr) {
        int count = 0;
        for(int i = 0; i < arr.size() - 2; i++) {
            for(int j = 1 + i; j < arr.size() - 1; j++) {
                if(arr.get(j) - arr.get(i) == d) {
                    for(int k = j + 1; k < arr.size(); k++) {
                        if(arr.get(k) - arr.get(j) == d) count++;
                    }
                }
            }
        }
        return count;
    }

    public static int beautifulTriplets3(int d, List<Integer> arr) {
        int count = 0;
        Map<Integer,Integer> integerMap = new HashMap<>();

        for (int i = 0; i < arr.size(); i++) {
            integerMap.put(arr.get(i),i);
        }

        for (int i = 1; i < arr.size(); i++) {
            int before = arr.get(i) - d;
            if (integerMap.containsKey(before) && integerMap.get(before) < i) {
                int after = arr.get(i) + d;
                if (integerMap.containsKey(after) && integerMap.get(after) > i) {
                    ++count;
                }
            }
        }
        return count;
    }
}
