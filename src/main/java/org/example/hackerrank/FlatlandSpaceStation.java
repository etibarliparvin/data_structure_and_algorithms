package org.example.hackerrank;

import java.util.Arrays;

public class FlatlandSpaceStation {
    public static void main(String[] args) {

    }
    static int flatlandSpaceStations(int n, int[] c) {
        Arrays.sort(c);
        int max = 0;
        for (int i = 0; i < c.length - 1; i++) {
            int distance = c[i + 1] - c[i];
            if(distance > max) max = distance;
        }
        return Math.max(Math.max(c[0], max / 2), n - 1 - c[c.length - 1]);
    }
}
