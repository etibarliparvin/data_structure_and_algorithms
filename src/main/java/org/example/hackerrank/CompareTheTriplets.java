package org.example.hackerrank;

import java.util.ArrayList;
import java.util.List;

public class CompareTheTriplets {
    public static List<Integer> compareTriplets(List<Integer> a, List<Integer> b) {
        int winA = 0;
        int winB = 0;
        List<Integer> result = new ArrayList<>();
        for (int i = 0; i < a.size(); i++) {
            if (a.get(i) > b.get(i))
                winA++;
            else if (b.get(i) > a.get(i))
                winB++;
        }
        result.add(winA);
        result.add(winB);
        return result;
    }
}
