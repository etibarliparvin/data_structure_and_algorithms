package org.example.hackerrank;

public class SherlockAndSquares {
    public static void main(String[] args) {
        System.out.println(squares(35, 70));
        System.out.println(squares(100, 1000));
    }

    public static int squares(int a, int b) {
        int count = 0;
        int first = (int) Math.sqrt(a);
        int last = (int) Math.sqrt(b);
        if (first * first == a) count++;
        count = count + (last - first);
        return count;
    }
}
