package org.example.hackerrank;

public class JumpingOnTheCloudsRevisited {
    public static void main(String[] args) {
        int[] arr = {1, 1, 1, 0, 1, 1, 0, 0, 0, 0};
        System.out.println(jumpingOnClouds(arr, 3));
    }

    static int jumpingOnClouds(int[] c, int k) {
        int energy = 100;
        int n = c.length;
        int i = 0;
        do {
            i = (i + k) % n;
            if (c[i] == 1) energy -= 3;
            else energy -= 1;
        } while (i != 0);
        return energy;
    }
}

