package org.example.hackerrank;

import java.util.Collections;
import java.util.List;

public class MinimumAbsoluteDifferenceInAnArray {
    public static void main(String[] args) {
        System.out.println(minimumAbsoluteDifference(List.of(-2, 2, 4)));
        System.out.println(minimumAbsoluteDifference(List.of(3, -7, 0)));
        System.out.println(minimumAbsoluteDifference(List.of(-59, -36, -13, 1, -53, -92, -2, -96, -54, 75)));
    }

    public static int minimumAbsoluteDifference(List<Integer> arr) {
        int result = Math.abs(arr.get(0) - arr.get(1));
        Collections.sort(arr);
        for(int i = 0; i < arr.size() - 1; i++) {
            int difference = Math.abs(arr.get(i) - arr.get(i + 1));
            if(difference < result) result = difference;
        }
        return result;
    }
}
