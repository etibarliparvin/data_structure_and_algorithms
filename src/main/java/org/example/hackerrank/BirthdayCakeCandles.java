package org.example.hackerrank;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class BirthdayCakeCandles {
    public static int birthdayCakeCandles(List<Integer> candles) {
        // Write your code here
        Map<Integer, Integer> map = new HashMap<>();
        int max = candles.get(0);
        for (int i = 0; i < candles.size(); i++) {
            if (map.containsKey(candles.get(i))) {
                map.put(candles.get(i), map.get(candles.get(i)) + 1);
            } else {
                map.put(candles.get(i), 1);
            }
            if (candles.get(i) > max)
                max = candles.get(i);
        }
        return map.get(max);
    }
}
