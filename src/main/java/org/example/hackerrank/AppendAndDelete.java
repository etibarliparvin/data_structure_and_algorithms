package org.example.hackerrank;

import java.util.HashMap;
import java.util.Map;

public class AppendAndDelete {
    public static void main(String[] args) {
        System.out.println(appendAndDelete2("p", "pu", 2));
        System.out.println(appendAndDelete2("qwerasdf", "qwerbsdf", 6));
    }

    public static String appendAndDelete(String s, String t, int k) {
        int result = 0;
        Map<Character, Integer> map = new HashMap<>();
        for (int i = 0; i < s.length(); i++) {
            if (map.containsKey(s.charAt(i))) {
                map.put(s.charAt(i), map.get(s.charAt(i)) + 1);
            } else {
                map.put(s.charAt(i), 1);
            }
        }
        for (int i = 0; i < t.length(); i++) {
            if (map.containsKey(t.charAt(i))) {
                map.put(t.charAt(i), map.get(t.charAt(i)) - 1);
            } else {
                result++;
            }
        }
        for (Integer i : map.values()) {
            result += Math.abs(i);
        }
        return k >= result ? "Yes" : "No";
    }

    public static String appendAndDelete2(String s, String t, int k) { // k = 2
        int sLenght = s.length(); // 1
        int tLenght = t.length(); // 2
        if (k >= sLenght + tLenght) return "Yes";
        int length = Math.min(sLenght, tLenght); // 1
        int i = 0;
        for (; i < length; i++) { // 0 < 1
            if (s.charAt(i) != t.charAt(i))
                break;
        }
        k -= sLenght - i; // 2 -=  0
        k -= tLenght - i; // 2 -= 1
        // k = 1
        return (k >= 0 && k % 2 == 0) ? "Yes" : "No";
    }
}
