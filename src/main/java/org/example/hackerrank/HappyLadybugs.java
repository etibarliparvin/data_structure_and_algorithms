package org.example.hackerrank;

import java.util.HashMap;
import java.util.Map;

public class HappyLadybugs {
    public static void main(String[] args) {
//        System.out.println(happyLadybugs("G")); // NO
//        System.out.println(happyLadybugs("GR")); // NO
//        System.out.println(happyLadybugs("_GR_"));
//        System.out.println(happyLadybugs("_R_G_"));
//        System.out.println(happyLadybugs("R_R_R"));
        System.out.println(happyLadybugs("RRGGBBXX")); // NO
//        System.out.println(happyLadybugs("RRGGBBXY"));
    }

    public static String happyLadybugs2(String b) {
        if (b.length() == 1 && b.charAt(0) != '_') return "NO";
        if (b.length() == 1 && b.charAt(0) == '_') return "YES";

        Map<Character, Integer> map = new HashMap<>();
        boolean hasSpace = false, hasUnhappy = false;

        /*
         sagindan ve solundan eyni anda ferqliliyini yoxlayir
         eger her iki terefinden ferqlidirse true eks halda false
         */
        for (int i = 1; i < b.length() - 1; i++) {
            if (b.charAt(i) != b.charAt(i - 1) && b.charAt(i) != b.charAt(i + 1)) hasUnhappy = true;
        }

        for (int i = 0; i < b.length(); i++) {
            if (b.charAt(i) == '_')
                hasSpace = true;
            else {
                if (map.containsKey(b.charAt(i)))
                    map.put(b.charAt(i), map.get(b.charAt(i)) + 1);
                else
                    map.put(b.charAt(i), 1);
            }
        }
        if (!hasSpace && hasUnhappy) return "NO";
        for (Map.Entry<Character, Integer> entry : map.entrySet()) {
            if (entry.getValue() <= 1)
                return "NO";
        }
        return "YES";
    }

    public static String happyLadybugs(String b) {
        if (b.length() == 1 && b.charAt(0) != '_') return "NO";
        if (b.length() == 1 && b.charAt(0) == '_') return "YES";
        Map<Character, Integer> map = new HashMap<>();
        for (int i = 0; i < b.length(); i++) {
            if (!map.containsKey(b.charAt(i))) {
                map.put(b.charAt(i), 1);
            } else {
                map.put(b.charAt(i), map.get(b.charAt(i)) + 1);
            }
        }
        boolean flag = true;
        if (!map.containsKey('_')) {
            for (int i = 1; i < b.length() - 1; i++) {
                if (b.charAt(i) == b.charAt(i - 1) || b.charAt(i) == b.charAt(i + 1))
                    flag = true;
                else flag = false;
            }
            if (flag) return "YES";
            else return "NO";
        } else {
            map.remove('_');
            if (map.containsValue(1)) return "NO";
            else return "YES";
        }
    }
}
