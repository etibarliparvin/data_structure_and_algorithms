package org.example.hackerrank;

import java.util.List;

public class LisasWorkbook {
    public static void main(String[] args) {
        System.out.println(workbook(5, 3, List.of(4, 2, 6, 1, 10)));
        System.out.println(workbook(15, 20, List.of(1, 8, 19, 15, 2, 29, 3, 2, 25, 2, 19, 26, 17, 33, 22)));
    }

    public static int workbook(int n, int k, List<Integer> arr) {
        int count = 0;
        int page = 1;
        for (int i = 0; i < arr.size(); i++) {
            int temp = 1;
            int element = arr.get(i);
            while (temp <= element) {
                if (temp == page) count++;
                if (temp % k == 0)
                    page++;
                temp++;
            }
            if (--temp % k != 0) page++;
        }
        return count;
    }
}
