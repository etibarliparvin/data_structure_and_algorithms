package org.example.hackerrank;

import java.util.Collections;
import java.util.List;

public class MigratoryBirds {
    public static int migratoryBirds(List<Integer> arr) {
        Collections.sort(arr);
        int max = 0;
        int count = 0;
        int element = arr.get(0);
        for (int i = 0; i < arr.size() - 1; i++) {
            if (arr.get(i) == arr.get(i + 1)) {
                count++;
            } else {
                count = 0;
            }
            if (count > max) {
                max = count;
                element = arr.get(i);
            }
        }
        return element;
    }
}
