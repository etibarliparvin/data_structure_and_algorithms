package org.example.hackerrank;

public class FindDigits {
    public static void main(String[] args) {
        System.out.println(5 % 0);
    }
    public static int findDigits(int n) {
        int number = n;
        int count = 0;
        while (n != 0) {
            int digit = n % 10;
            if (digit != 0 && number % digit == 0) count++;
            n /= 10;
        }
        return count;
    }
}
