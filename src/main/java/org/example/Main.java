package org.example;

import java.util.HashMap;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        int n = 7;
        List<Integer> list = List.of(1, 2, 1, 2, 1, 3, 2);
        System.out.println(sockMerchant(n, list));
    }

    public static int[] twoSum(int[] nums, int target) {
        int[] result = {-1, -1};
        HashMap<Integer, Integer> map = new HashMap<>();
        for (int i = 0; i < nums.length; i++) {
            if (map.containsKey(target - nums[i])) {
                result[0] = map.get(target - nums[i]);
                result[1] = i;
                break;
            }
            map.put(nums[i], i);
        }
        return result;
    }

    public static int sockMerchant(int n, List<Integer> ar) {
        int[] res = new int[101];
        int pairs = 0;
        for (Integer el : ar) {
            res[el]++;
        }
        for (int i = 0; i < 101; i++) {
            pairs += res[i] / 2;
        }
        return pairs;
    }

    public static int sockMerchant2(int n, List<Integer> ar) {
        HashMap<Integer, Integer> map = new HashMap<>();
        int res = 0;
        for (Integer el : ar) {
            if (map.containsKey(el)) {
                map.put(el, map.get(el) + 1);
            } else {
                map.put(el, 1);
            }
        }
        for (Integer key : map.keySet()) {
            res += map.get(key) / 2;
        }
        return res;
    }


}